// Core, Team, and Official extensions can `require` VM code:
const ArgumentType = require('../../extension-support/argument-type');
const BlockType = require('../../extension-support/block-type');

class textBlocks {
    getInfo () {
        return {
            id: 'celleron56 costum extensions',
            name: 'Some Blocks',
            blocks: [
                {
                    opcode: 'myReporter',
                    blockType: BlockType.REPORTER,
                    text: 'letters [LETTER_NUM] to [LETTER_END] of [TEXT]',
                    arguments: {
                        LETTER_NUM: {
                            type: ArgumentType.STRING,
                            defaultValue: '1'
                        },
                        LETTER_NUM: {
                            type: ArgumentType.STRING,
                            defaultValue: '1'
                        },
                        TEXT: {
                            type: ArgumentType.STRING,
                            defaultValue: 'text'
                        }
                    }
                },
                {
                    opcode: 'replaceblock',
                    blockType: BlockType.REPORTER,
                    text: 'replace  [OLD] with [NEW] in [TEXT]',
                    arguments: {
                        LETTER_NUM: {
                            type: ArgumentType.STRING,
                            defaultValue: 't'
                        },
                        LETTER_NUM: {
                            type: ArgumentType.STRING,
                            defaultValue: 'e'
                        },
                        TEXT: {
                            type: ArgumentType.STRING,
                            defaultValue: 'text'
                        }
                    }
                }
            ]
        };
    }
}
class SomeBlocks {
    // ...
    myReporter (args) {
        return args.TEXT.slice(args.LETTER_NUM , args.LETTER_END);
    },
     replaceblock (args) {
        return args.TEXT.replace(args.OLD , args.NEW);
    };
    // ...
    
}